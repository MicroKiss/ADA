generic	
	type Index is (<>);
	type Counter is (<>);
	
package Bag_Package is

	type Bag is private;
	
	
	procedure Empty(B: in out Bag);
	procedure Add(B: in out Bag;I: Index);
	procedure Remove(B: in out Bag;I: Index);
	function Has_Any(B: in out Bag;I: Index) return Boolean;
	
private
	type Bag_Array is array(Index range <>) of Counter;
	type Bag is record
		Container: Bag_array(Index'First..Index'Last);
--		Position: Index := Index'First;
	end record;

end Bag_Package;

--	__  ___                  _   __           //
--	|  \/  (_)              | | / (_)         //
--	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
--	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
--	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
--	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //
