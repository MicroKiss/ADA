with Bag_package, Ada.Integer_Text_IO, Ada.Text_IO,Ada.Numerics.Discrete_Random ;
with Map_package,Warrior_package;
use  Ada.Integer_Text_IO, Ada.Text_IO;

procedure Simulation is
	
	type Field is (Empty, Stone, Wood, Sword, Water, Trap, Enemy);
	subtype Resource is Field range Stone..Sword;
	subtype Obstacle is Field range Water..Enemy;
	
	package Inventory_Bag is new Bag_package(
		Index    => Resource,
		Counter => Natural
	);
	use Inventory_Bag;
	
		function Tool(O: in Obstacle)return Resource is
	begin
			case O is
			when Water =>
			return Stone;
			when Trap =>
			return Wood;
			when Enemy =>
			return Sword;
			--when others =>
			--null;
		end case;
	end Tool;
	
	package My_Warrior is new Warrior_package(
		Bag    => Inventory_Bag.Bag,
		Resource => Resource,
		Obstacle => Obstacle,
		Tool =>Tool,
		Empty =>Inventory_Bag.Empty,
		Add => Inventory_Bag.Add,
		Remove => Inventory_Bag.Remove,
		Has_Any => Inventory_Bag.Has_Any
	);
	use My_Warrior;
	
		procedure Field_Action(F: in out Field;W: in out Warrior) is
	begin
	if (F = Field'First) then
		return;
	elsif (F >=Stone and F <= Sword) then
		Collect(W,F);
		Put_Line("The player has collected some "& Field'Image(F));
	elsif (F >= Water and F <= Enemy) then
		Fight(W,F);
		Put_Line("The player has fought "& Field'Image(F));
	end if;
	F := Field'First;
		
	end;
	
	package My_Map is new Map_package(
	Warrior=> Warrior,
	Field=> Field,
	Field_Action =>Field_Action,
	Empty=> Field'First
	);
use My_Map;
	
	

Player: Warrior;
Map: My_Map.Map(3);

	
begin
	Init(Map);
	Init(Player);
	
	While ( not Is_Empty(Map)  and Is_Alive(Player)) loop
		move(Map,Player);
		--Put(Map);
		--Put_Line("");
	end loop;
	
	if(Is_Alive(Player)) then
	Put(" Player has WON the game yaaay ^^");
	else
	Put("The payer LOST the game  #sadface");
	end if;
	
end Simulation;

--	__  ___                  _   __           //
--	|  \/  (_)              | | / (_)         //
--	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
--	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
--	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
--	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //
