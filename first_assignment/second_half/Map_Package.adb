with Ada.Integer_Text_IO, Ada.Text_IO,Ada.Numerics.Discrete_Random ;
use  Ada.Integer_Text_IO, Ada.Text_IO;

package body Map_Package is 

	procedure Init(M: in out Map) is
	
	package Field_Random is new Ada.Numerics.Discrete_Random(Field);
		--generator
	Dice: Field_Random.Generator;
	begin
		Field_Random.Reset(Dice);
		for I in M.Fields'Range loop
			for J in M.Fields'Range(2) loop
				M.Fields(I,J) := Field_Random.Random(Dice);
			end loop;
		end loop;
	end Init;
	--------------------------------------------------------------------
	
	procedure Put(M: in out Map) is
	begin
		For I in M.Fields'Range(1) loop
			For J in M.Fields'Range(2) loop
				Put(Field'Image(M.Fields(I,J))& " ");
			end loop;		
			Put_Line("");
		end loop;
		
	end Put;
	--------------------------------------------------------------------

	procedure Move(M: in out Map;W: in out Warrior) is
	
	type one_to_four is range 1..4;
	
	package Direction_Random is new Ada.Numerics.Discrete_Random(one_to_four);
		--generator
	Dice: Direction_Random.Generator;
	random: one_to_four;
	begin
		--seed the randomness
	Direction_Random.Reset(Dice);
	random:= Direction_Random.random(Dice);
		case random is
			when 1 =>
				M.Position.x :=(M.Position.x+1) mod M.Fields'Length;
			when 2=>
				M.Position.y :=(M.Position.y-1) mod M.Fields'Length;
			when 3=>
				M.Position.x :=(M.Position.x-1) mod M.Fields'Length;
			when 4=>
				M.Position.y :=(M.Position.y+1) mod M.Fields'Length;
			--when others =>
			--null;
		end case;
		--Put_Line(Integer'Image(M.Fields'Length));
		--Put_Line(Integer'Image(M.Position.x) & " " & Integer'Image(M.Position.x));
		
		
		Field_Action(M.Fields(M.Position.x+1,M.Position.y+1), W);
	end Move;
	--------------------------------------------------------------------
	
	function Is_Empty(M: in out Map) return Boolean is
	B : Boolean := TRUE;
	begin
		
		for I in M.Fields'range(1) loop
			for J in M.Fields'range(2) loop
				if M.Fields(I,J) /= Empty then
					B := FALSE;
				end if;
			end loop;
		end loop;
		
	return B;
	end Is_Empty;
	
end Map_Package;

--	__  ___                  _   __           //
--	|  \/  (_)              | | / (_)         //
--	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
--	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
--	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
--	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //