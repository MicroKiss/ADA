with Bag_package,Ada.Integer_Text_IO, Ada.Text_IO,Ada.Numerics.Discrete_Random ;
use  Ada.Integer_Text_IO, Ada.Text_IO;




package body Warrior_Package is 

	procedure Init(W: in out Warrior) is
	begin
		For I in Resource'Range loop
			Add(W.Inventory,I); 
		end loop;
	end Init;
	--------------------------------------------------------------------
	
	procedure Fight(W: in out Warrior;O: in Obstacle) is
	begin
		if (Has_Any(W.Inventory,Tool(O))) then
			Remove(W.Inventory,Tool(O));
		else	
			W.Status := FALSE;
		end if;

		
	end Fight;
	--------------------------------------------------------------------

	procedure Collect(W: in out Warrior;R: in Resource) is
	begin
		Add(W.Inventory,R);
	
	end Collect;
	--------------------------------------------------------------------
	
	function Is_Alive(W: in out Warrior) return Boolean is
	begin
		return W.Status;
	end Is_Alive;
	
end Warrior_Package;

--	__  ___                  _   __           //
--	|  \/  (_)              | | / (_)         //
--	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
--	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
--	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
--	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //