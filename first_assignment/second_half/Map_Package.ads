generic	
	type Field is (<>);
	type Warrior is private;
	with procedure Field_Action(F: in out Field;W: in out Warrior);
	Empty: Field;
	
package Map_Package is

	type Map(Size : Natural) is private;
	
	
	procedure Init(M: in out Map);
	procedure Put(M: in out Map);
	procedure Move(M: in out Map;W: in out Warrior);
	function Is_Empty(M: in out Map) return Boolean;
	
	
		type Matrix is array(Natural range <>,Natural range <>) of Field;
private
	type coord is record
		x: Natural:= 0;
		y: Natural:= 0;
	end record;
	

	
	type Map(Size : Natural) is record
		Fields: Matrix(1..Size,1..Size);
		Position: coord;
	end record;

end Map_Package;

--	__  ___                  _   __           //
--	|  \/  (_)              | | / (_)         //
--	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
--	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
--	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
--	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //