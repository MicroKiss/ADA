package body Bag_Package is 

	procedure Empty(B: in out Bag) is
	begin
		for I in B.Container'Range loop
			B.Container(I) := Counter'First;
		end loop;
	end Empty;
	--------------------------------------------------------------------
	
	procedure Add(B: in out Bag;I: Index) is
	begin
		if (B.Container(I) /= Counter'Last ) then
			B.Container(I) := Counter'Succ(B.Container(I));
		end if;
		
	end Add;
	--------------------------------------------------------------------

	procedure Remove(B: in out Bag;I: Index) is
	begin
		if (B.Container(I) /= Counter'First) then
			B.Container(I) := Counter'Pred(B.Container(I));
		end if;
	end Remove;
	--------------------------------------------------------------------
	
	function Has_Any(B: in out Bag;I: Index) return Boolean is
	begin
		return (B.Container(I) > Counter'First);
	end Has_Any;
	
end Bag_Package;

--	__  ___                  _   __           //
--	|  \/  (_)              | | / (_)         //
--	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
--	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
--	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
--	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //