with Bag_package, Ada.Integer_Text_IO, Ada.Text_IO,Ada.Numerics.Discrete_Random ;
use  Ada.Integer_Text_IO, Ada.Text_IO;

procedure Test is
	
	type one_to_six is range 1..6;
	
	package Dice_Bag is new Bag_package(
		Index    => one_to_six,
		Counter => Natural
	);
	use Dice_Bag;
	
	package Dice_Random is new Ada.Numerics.Discrete_Random(one_to_six);
	
	--generator
	Dice: Dice_Random.Generator;
	
	B: Dice_Bag.Bag;
	--TMP: one_to_six;
begin
		--seed the randomness
	Dice_Random.Reset(Dice);
	Empty(B);
	-- feltoltjuk 20 dobassal
	for I in  1..20 loop
		Add(B,Dice_Random.Random(Dice));
		
	--	TMP := Dice_Random.Random(Dice);
	--	Add(B,TMP);
	--	Put(one_to_six'image(TMP));
	end loop;
	
	--put_Line("");
	
	declare --declare1
	 Found : Boolean := FALSE;
	begin
		while not Found loop ---while loop
			for I in one_to_six'Range loop
				Remove(B,I);
				if (not Has_Any(B,I)) then
					Put(one_to_six'image(I));
					Found := TRUE;
				end if;
			end loop;--for loop
		end loop; --while loop
	end;  --declare1
	
end test;

--	__  ___                  _   __           //
--	|  \/  (_)              | | / (_)         //
--	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
--	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
--	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
--	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //