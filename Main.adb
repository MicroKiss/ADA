with Ada.Text_IO,Ada.Integer_Text_IO;
use Ada.Text_IO,Ada.Integer_Text_IO;

procedure Main is

	function lnko(A: in InTeGeR;B: in InTeGeR) return Integer is
	begin
	if  (A mod B) = 0	 then
		return B;
		
	else
		return lnko(B,A mod B);
		
	end if;
	end lnko;

X: Integer;
Y: Integer;

begin
put_line("Add meg a ket szamot aminek ki szeretned szamolni az lnko-jat");
Get(X);Get(Y);
Put("A ket szam legnagyobb kozos osztoja: " );Put(lnko(X,Y));
end Main;